
. Installation

Please, check the doc/index.html file with your browser
for more information.

When installing on your server, feel free to delete the
doc folder, it is only for developers.

WARNING: Do not forget to make a backup of your Database.
         This module has the potential of removing tags
	 from your nodes...

Thank you.
Made to Order Software Corp.

