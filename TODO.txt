
Add one checkbox in nodes so one can avoid MO Auto-add terms on
that node (and a flag in the taxonomy settings to know whether that
flag should be shown.)

Add a field of false positive so we can avoid having the wrong terms
in such and such page. (i.e. if present in false positive, do not add
that term whether or not it appears in the node.)

Add a field of "positives": terms that we always want attached to the
node whether or not they appear in the node, but leave the rest to
the MO Auto add terms system (I think this can be achieved with the use
of another taxonomy though...)

Add a flag in the Term edit page so a term can be marked as "Explicit."
Meaning that if you do not specify it in the list of terms, even if it
exists in the node, it will not be added to the node.

Add support for tags title="...", alt="...", value="..."

Implement a fix for lists of tags that grow to more than 1024 bytes.

Fix the problem with Preview which it seems uses the taxonomy "array"
before it is an array... This does not seem to be our problem, yet
the error happens only when our module is enabled.

Consider a timer to run the CRON processes between X and Y time
instead of each time CRON runs. This can be achieve with the advanced
CRON module as far as I know.

AJAX to pre-parse the tags while editing... we would want a button
which when clicked sends the node content to the server which in
turn returns the list of terms found (as if we had saved the node)
and then adds those terms to the taxonomy field. This sounds quite
complicated to me! However, this could be used as a "There are the
suggested tags"

Speaking of which, it would be good to have a way to select just
a small set of those terms and not re-auto-assign terms later.
(this is now do-able, although automated, with the use of the
Maximum count and setting the CRON count to 0. Of course, since it
is automatic, a count of 10 would do too, it'd just update the
nodes as required as the taxonomy changes...)

Review the clearing of our cache because for sites with a large
number of nodes it can be real slow (i.e. it needs to go through
all the nodes.)

